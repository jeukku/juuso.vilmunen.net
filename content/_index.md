My activism and voluntary work is currently focused on [Koto co-op](https://kotocoop.org/)

My social media is only at [scuttlebutt](https://scuttlebutt.nz/). It is a social network and a decentralized platform. Here are my profiles: [life and activism](https://room.kotocoop.org/alias/jeukkumobile) and [computers and coding](https://room.kotocoop.org/alias/jeukku)

If you see somebody called "jeukku" on various chat platforms, it might be me.
